## Basic-Usage:
    
      s1-labeler [options] [TARGET1] [TARGET2] ...

where `TARGET` can be a prefix for multiple S1-endpoints, or a specific endpoint

## Description
A command-line tool for adding external-ID's to SentinelOne endpoints using the SentinelOne, vSphere, and Confluence API's

## Options

### -t,  --tag
      
explicitly define a tag for the preceding prefix (without consulting vsphere)

_Note:_ tags containing the strings 'aus' or 'dal' will automatically be replaced for each enpoint based on 
the first letter of the endpoint's name  ('a' vs 'd')

example:
      
      s1-labeler [TARGET] -t tag_to_apply_verbatim

### -f,  --format
      
supply a new external-ID format to be constructed from vSphere, (using vSphere tag-categories) 
each category must be seperated by the '|' character
The default format is 'application|nomad_node_class|location|environment'

_Note:_ the '-t' option will override this format for the specific prefix, if supplied

example: 
      
      s1-labeler -f vSphere-category1|vSphere-category2|vSphere-category3| [TARGET1] [TARGET2]

### -y,  --yes
      
do not prompt user to confirm changes before committing (only usable in conjunction with '-t' option)

### -a,  --all
      
pulls all prefixes from the table on this [confluence page](https://confluence.q2ebanking.com/x/Od7QBQ) 
       
### -h,  --help
      
display this help message
