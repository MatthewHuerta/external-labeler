#!/usr/bin/python3

from logging import NOTSET
import requests
import json
import colorama
from requests.auth import HTTPBasicAuth
from requests.models import Request
import urllib3
import os
import re
from bs4 import BeautifulSoup
import sys
import math
import re

help_message = '''
Basic-Usage:
    
    s1-labeler [options] [TARGET1] [TARGET2] ...
    where "TARGET" can be a prefix for multiple S1-endpoints, or a specific endpoint

Description:
    A command-line tool for adding external-ID's to SentinelOne endpoints using the SentinelOne, vSphere, and Confluence API's
    If no tag is explicitly defined using the '-t' option, construction of one will be attempted using vSphere tags
    The default external-ID constructed from vSphere is of the format 'application|nomad_node_class|location|environment', 
    and can be overwritten using the '-f' option as shown below

Options: 

  -t --tag
        explicitly define a tag for the preceding prefix (without even consulting vsphere)
        NOTE: tags containing the strings 'aus' or 'dal' will automatically be replaced for each enpoint based on 
        the first letter of the endpoint's name  ('a' vs 'd')
        example:
                    $ s1-labeler [TARGET] -t tag_to_apply_(literally)
  -f --format
        supply a new external-ID format to be constructed from vSphere, using vSphere tag-categories seperated by '|' character
        The default format is 'application|nomad_node_class|location|environment'
        Note: the '-t' option will override this format for that specific prefix, if supplied.
        example:
                    $ s1-labeler -f vSphere-tag-category1|vSphere-tag-category2|vSphere-tag-category3| [TARGET1] [TARGET2]
  -y --yes
        do not prompt user to confirm changes before committing (only usable in conjunction with '-t' option)
  -a --all
        pulls all prefixes from the first two html tables located at https://confluence.q2ebanking.com/x/Od7QBQ        
  -h --help
        display this help message

  
        

'''


def modify_for_location(computer : str, id : str) :
    id = str(id)
    if computer[0] == 'a' :
        return id.replace('dal', 'aus')
    if computer[0] == 'd' :
        return id.replace('aus', 'dal') 

def print_table (headers : list, content : list) : 
    t_size = os.get_terminal_size().columns
    field_width = []
    for i in range(0, len(headers)) :
        field_width.append(len(headers[i]))
    for i in range(0, len(content)):
        if len(headers) != len(content[i]) :
            print(colorama.Fore.RED + 'header array and content array are different lengths for content[', colorama.Fore.CYAN + str(i), colorama.Fore.RED + ']',\
                colorama.Fore.RESET, sep='')
            print(headers)
            print(content[i])
            return -1
        for j in range(0, len(headers) - 1) :
            if len(content[i][j]) > field_width[j] :
                field_width[j] = len(content[i][j])
    format_str = '{:^1}'
    colored_str = '{:^1}'
    fill_str = '{:^1}'
    fill_str2 = '{:^1}'
    total_width = 1
    # print(colorama.Fore.CYAN + 'CYAN:', colorama.Fore.RESET, len(colorama.Fore.CYAN))
    # print(colorama.Fore.RED + 'RED:', colorama.Fore.RESET, len(colorama.Fore.RED))
    # print(colorama.Fore.YELLOW + 'YELLOW:', colorama.Fore.RESET, len(colorama.Fore.YELLOW))
    # print(colorama.Fore.BLUE + 'BLUE:', colorama.Fore.RESET, len(colorama.Fore.BLUE))
    # print(colorama.Fore.RESET + 'RESET:', colorama.Fore.RESET, len(colorama.Fore.RESET))
    for i in field_width :
        total_width += i - 1
    scale_factor = t_size/total_width
    # print('field-width:', field_width, '\nt_size:', t_size, '\ntotal_width:', total_width,'\nscale-factor:', scale_factor)
    for i in field_width:
        i *= scale_factor
        i = math.floor(i) - 1
        colored_str += '{:^' + str(i - 2 + 10) + '}{:^1}'
        format_str += '{:^' + str(i - 2) + '}{:^1}'
        fill_str += '{:_>' + str(i - 2) + '}{:^1}'
        fill_str2 += '{: >' + str(i - 2) + '}{:^1}'
    print_str = ['_']
    for i in range(0, len(headers)):
        print_str.append('_')
        print_str.append('_')
    print(fill_str.format(*print_str))
    print_str = ['|']
    for i in range(0, len(headers)):
        print_str.append(' ')
        print_str.append('|')
    print(fill_str2.format(*print_str))
    print_str = ['|']
    for i in range(0, len(headers)):
        print_str.append(colorama.Fore.CYAN + headers[i] + colorama.Fore.RESET)
        print_str.append('|')
    print(colored_str.format(*print_str))
    print_str = ['|']
    for i in range(0, len(headers)):
        print_str.append('_')
        print_str.append('|')
    print(fill_str.format(*print_str))
    # s1_set_external_id(k["computerName"], external_id)
    for i in range(0, len(content)):
        print_str = ['|']
        for j in range(0, len(content[i])):
            print_str.append(content[i][j])
            print_str.append('|')
        print(format_str.format(*print_str))
        print_str = ['|']
    for i in range(0, len(headers)):
        print_str.append('_')
        print_str.append('|')
    print(fill_str.format(*print_str))

### Variables ###

try :
    VSPHERE_USER = os.environ["VSPHERE_USER"]
except (KeyError) :
    VSPHERE_USER = input('enter Vcenter username (or define VSPHERE_USER in environment variable)\n')
    
try :
    VSPHERE_PASSWORD = os.environ["VSPHERE_PASSWORD"]
except (KeyError) :
    VSPHERE_PASSWORD = input('enter Vcenter password (or define VSPHERE_PASS in environment variable)\n')

try :
    S1_API_TOKEN = os.environ["S1_API_TOKEN"]
except (KeyError) :
    S1_API_TOKEN = input('enter S1 API key (or define SI_API_KEY in environment variable)\n')

try :
    CONFLUENCE_USER = os.environ["CONFLUENCE_USER"]
except (KeyError) :
    CONFLUENCE_USER = input('enter Confluence username (or define CONFLUENCE_USER in environment variable)\n')

try :
    CONFLUENCE_PASS = os.environ["CONFLUENCE_PASS"]
except (KeyError) :
    CONFLUENCE_PASS = input('enter Confluence password (or define CONFLUENCE_PASS in environment variable)\n')

session_confluence = requests.session()
session_vsphere_aus = requests.session()
session_vsphere_dal = requests.session()
session_s1 = requests.session()
    
vsphere_url_base_aus = 'https://aus-vcs-prd-01.q2dc.local'
vsphere_url_base_dal = 'https://dal-vcs-prd-01.q2dc.local'
s1_url_base = 'https://usea1-009.sentinelone.net'
confluence_base_url = 'https://confluence.q2ebanking.com'

colorama.init(autoreset=False)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def print_response_info(r: requests.Response) :
    print(colorama.Fore.BLUE + "response status code: ", r.status_code)
    print(colorama.Fore.BLUE + "encoding: ", r.encoding)
    print(colorama.Fore.BLUE + "headers: ", r.headers)
    print(colorama.Fore.BLUE + "reason: ", r.reason)
    print(colorama.Fore.BLUE + "cookies: ", r.cookies)
    print(colorama.Fore.BLUE + "url: ", r.url)
    print(colorama.Fore.BLUE + "request: ", r.request)
    # print(colorama.Fore.BLUE + "text: ", r.text)
    if r.json() != None :
        pretty_string = json.dumps(r.json(), indent=2)
        print(colorama.Fore.BLUE + "body (in JSON): \n", pretty_string)

### given a substring of the hostname returns a list object ###
def s1_get_agents(agent_hostname: str, verbose=False) :  
    agent_hostname = agent_hostname.replace('\n', '')
    queries = {
    'computerName__like': agent_hostname
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": 'ApiToken ' + S1_API_TOKEN
    }
    
    r = session_s1.get(url=s1_url_base + '/web/api/v2.1/agents', headers=headers, params=queries)

    response_dict = json.loads(r.text)

    if verbose == True :
        print_response_info(r)
    host_info = []
    for i in response_dict['data'] :
        if verbose == True:
            print(i['computerName'])
        dictionary = {
            'computerName' : i['computerName'],
            'externalId' : i['externalId']
        }
        host_info.append(dictionary)
        # if i['isActive'] == True :    
            # host_info.append(dictionary)
    return host_info

def s1_set_external_id(computerName : str, externalId : str, verbose=False) :
    headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": 'ApiToken ' + S1_API_TOKEN
    }
    payload = {
        'data' : {
            'externalId' : externalId
        },
        'filter' : {
            'computerName': computerName
        }
    }

    req = Request('POST', url=s1_url_base + '/web/api/v2.1/agents/actions/set-external-id', json=payload, \
            headers=headers)
    ready = req.prepare()
    r = session_s1.send(ready, verify=False)
    if verbose == True :
        print_response_info(r)
    if r.status_code != 200 :
        return r.status_code
    else :
        return 0

def vsphere_start_session(verbose=False) :

    ### start austin-dc session ###
     
    session_vsphere_aus.auth = HTTPBasicAuth(VSPHERE_USER, VSPHERE_PASSWORD)

    session_vsphere_aus.headers.update({"Accept": "application/json", "Content-Type": "application/json"})

    r = session_vsphere_aus.post(url=vsphere_url_base_aus + '/api/session', verify=False)
    
    session_vsphere_aus.headers.update({'vmware-api-session-id' : r.headers['vmware-api-session-id']})

    ### start dallas-dc session ###

    session_vsphere_dal.auth = HTTPBasicAuth(VSPHERE_USER, VSPHERE_PASSWORD)

    session_vsphere_dal.headers.update({"Accept": "application/json", "Content-Type": "application/json"})

    r2 = session_vsphere_dal.post(url=vsphere_url_base_dal + '/api/session', verify=False)
    
    session_vsphere_dal.headers.update({'vmware-api-session-id' : r2.headers['vmware-api-session-id']})

    if verbose == True :
        print_response_info(r)
        print_response_info(r2)
    
### returns a dictionary in the format { 'tag' : 'category' } ###
def vsphere_get_vm_tags(vmname: str, verbose=False) :
    params = {
        'names' : vmname
    }
    s = requests.session()
    url = ''
    location = ''
    if vmname.startswith('a') | vmname.startswith('A'):
        s = session_vsphere_aus 
        url_base = vsphere_url_base_aus
        location = 'aus'
    else :
        s = session_vsphere_dal
        url_base=vsphere_url_base_dal
        location = 'dal'

    
    r = s.get(url=url_base + '/api/vcenter/vm/', params=params, verify=False)
    

    if verbose == True :
        print_response_info(r)
    

    response_dict = json.loads(r.text)
    if len(response_dict) == 0:
        return -1

    body = { 
        'object_id' : {
            'id' : response_dict[0]['vm'],
            'type' : 'VirtualMachine'
        }
    }
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')

    
    r = s.post(url=url_base + '/api/cis/tagging/tag-association?action=list-attached-tags',\
            data=jsondataasbytes, verify=False)

    if verbose == True :
        print(colorama.Fore.CYAN + "tags for " + vmname)
        print_response_info(r)
    
    response_dict = json.loads(r.text)
    tags = {'location' : location}
    for i in response_dict :
        r = s.get(url=url_base + '/api/cis/tagging/tag/' + i, verify=False)
        tags_response_dict = json.loads(r.text)
        if verbose == True :
            print_response_info(r)
        r = s.get(url=url_base + '/api/cis/tagging/category/' + \
            tags_response_dict['category_id'], verify=False)
        categories_response_dict = json.loads(r.text)
      
        tags[categories_response_dict['name']] = tags_response_dict['name']
    return tags

        


    return
    print(tags)
    # if verbose == True :
    #     print_response_info(r)
    
def confluence_get_content (verbose=False) :
    headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    }
    params = {
            'expand' : 'body.storage'
    }
    session_confluence.auth=HTTPBasicAuth(CONFLUENCE_USER, CONFLUENCE_PASS)
    r = session_confluence.get(url= confluence_base_url + '/rest/api/content/97574457', headers=headers, params=params)
    if verbose == True :
        print_response_info(r)
    response_dict = json.loads(r.text)
    return response_dict['body']['storage']['value']
    
def parse_confluence_html (verbose=False) :
    html = confluence_get_content()
    soup = BeautifulSoup(html, 'html.parser')

    table = soup.tbody
    header = soup.h3
    server_prefixes = []
    
    while table != None :
        pattern_asterisk = re.compile('(\*)')
        row = table.find("tr")
        # print(colorama.Fore.GREEN + header.string) 
        while row != None :
            if row.find('td') == None:
                row = row.next_sibling
                continue
            elems = row.td.find_next_siblings("td") 
            for i in range(0, len(elems)) :
                # print(elems[i].string, end='|')
                if i == len(elems) - 1 and  pattern_asterisk.search(elems[len(elems) - 1].string) :
                    server_prefixes.append(elems[len(elems) - 1].string.strip('\*'))
            # print('\n')
            row = row.next_sibling
        table = table.find_next("tbody")
        if not header.find_next("h3"):
            return server_prefixes
        else:
            header = header.find_next("h3")

def compose_tag (machine_name : str, tag_format : str) :
    tags = vsphere_get_vm_tags(machine_name, verbose=False)
    if tags == -1:
        return -1
    new_tag=''
    for i in tag_format.split('|') :
        try :
            new_tag += tags[i] + '_'
            continue
        except KeyError:
            print('for the host \"', colorama.Fore.BLUE + machine_name, colorama.Fore.RESET, '\" enter a vSphere-tag-value for category \"', colorama.Fore.CYAN + i, colorama.Fore.RESET, '\"', sep='')
            new_tag += input() + '_'
    print('use external-ID \"', colorama.Fore.MAGENTA + new_tag.rstrip('_'), colorama.Fore.RESET, '\" ? (y/n)', sep='')
    response = input()
    if response == 'y':
        return new_tag.rstrip('_')
    else :
        return -1

def main() :
    vsphere_start_session()
    server_prefixes = []
    tags = {}
    prompt = True
    format = 'application|nomad_node_class|location|environment'
    if len(sys.argv) == 1 or sys.argv[1] == '-h' or sys.argv[1] == '--help' :
        print(help_message)
        sys.exit(0)
    elif sys.argv[1] == '-a' or sys.argv[1] == '--all':
        server_prefixes = parse_confluence_html()
    elif len(sys.argv) > 1 :
        for i in range(1, len(sys.argv)) :
            if sys.argv[i-1] == '-f' or sys.argv[i-1] == '--format':
                continue
            elif sys.argv[i] == '-f' or sys.argv[i] == '--format':
                format = sys.argv[i+1]
            elif sys.argv[i] == '-y' or sys.argv[i] == '--yes':
                prompt = False
            elif sys.argv[i-1] == '-t' or sys.argv[i-1] == '--tag':
                continue
            elif sys.argv[i] == '-t' or sys.argv[i] == '--tag':
                tags.update({sys.argv[i-1] : sys.argv[i+1]})
            else:
                server_prefixes.append(sys.argv[i])    
    hosts = {}
    for i in server_prefixes :
        machines = s1_get_agents(i, verbose=False)
        hosts.update({i : machines})
        if len(machines) == 0 :
            print(colorama.Fore.RED + 'No S1-agents found for the prefix ', colorama.Fore.RESET, '\"', colorama.Fore.RESET, colorama.Fore.CYAN + i, colorama.Fore.RESET, '\"', sep='')    
        elif i in tags.keys() :
            for k in machines:
                k.update({'new_ID' : tags[i]})
        else:
            print(colorama.Fore.CYAN + i, ':', colorama.Fore.RESET)
            apply_tag_to_all_in_prefix = False
            # 'k' is a list of dictionaries with 2 elements: computerName, externalId
            for k in machines:
                if apply_tag_to_all_in_prefix == False :
                    while True :
                        if k['externalId'] != '' :
                            print("\tfor host \"", colorama.Fore.BLUE + k['computerName'], colorama.Fore.RESET,\
                                "\" modify current S1 external-ID \"", colorama.Fore.YELLOW + k['externalId'],colorama.Fore.RESET,  "\" ? (y/n)", sep='')
                            response = input('\t')
                            if response == 'n' :
                                break
                        external_id = compose_tag(k['computerName'], format)    
                        if external_id != -1 and k['externalId'] != external_id :
                            k.update({'new_ID' : external_id})
                            break
                        print("\tenter new external-ID for host \"", colorama.Fore.BLUE + k['computerName']\
                            , colorama.Fore.RESET, '\"', sep='')
                        response = input('\t')
                        k.update({'new_ID' : response})
                        break
                            
                    print("\tapply this ID-format to all endpoints in this prefix? (location-value will be changed based on vSphere \'location\' category tag) (y/n)", ':', sep='')
                    response = input('\t')
                    if response == 'y' :
                        try :
                            apply_tag_to_all_in_prefix = k['new_ID']
                        except KeyError:
                            apply_tag_to_all_in_prefix = k['externalId']
                else :
                    new_id = modify_for_location(k['computerName'], apply_tag_to_all_in_prefix)
                    if k['externalId'] != new_id :
                        print('\t applying external-ID \"', colorama.Fore.YELLOW + new_id, colorama.Fore.RESET,\
                            "\" to ", colorama.Fore.BLUE + k['computerName'], colorama.Fore.RESET, sep='')
                        k.update({'new_ID' : new_id})
    headers = ['PREFIX', 'VM_NAME', 'CURRENT_S1_EXTERNAL_ID', 'PROPOSED_S1_EXTERNAL_ID']
    for i in server_prefixes :
        counter = 0
        rows = []
        if len(hosts[i]) == 0 :
            continue
            column = []
            column = [i, 'No S1 agents', 'No S1 agents', 'No S1 agents']
            rows.append(column)
        else :
            for machines in hosts[i] :
                column = []
                counter += 1
                # first column
                if len(hosts[i]) == 1:
                    column.append('( ' + str(len(hosts[i])) + ' endpoint )')
                elif counter == math.floor((len(hosts[i]) / 2)) :
                    column.append(i)
                elif counter == math.floor((len(hosts[i]) / 2)) + 1 :
                    column.append('( ' + str(len(hosts[i])) + ' endpoints )')
                else :
                    column.append(' ')
                # second column
                column.append(machines['computerName'])
                # third column
                if machines['externalId'] == '' :
                    column.append('---')
                else :
                    column.append(machines['externalId'])
                # fourth column
                try : 
                    column.append(machines['new_ID'])
                except KeyError :
                    column.append('no change')
                rows.append(column)
        print_table(headers, rows)
    if prompt == True:
        print('\nmake the changes listed? (y/n)')
        response = input()
    else :
        response = 'y'
    if response == 'y':
        for i in server_prefixes :
            if len(hosts[i]) == 0 :
                continue
            else :
                for machines in hosts[i] :
                    if machines.get('new_ID') != None:       
                        print("for host \"", colorama.Fore.BLUE + machines['computerName'], colorama.Fore.RESET,\
                                "\" changing S1 external-ID to \"", colorama.Fore.YELLOW + machines['new_ID'],colorama.Fore.RESET,  '\" ...', sep='', end='')
                        result = s1_set_external_id(machines['computerName'], machines['new_ID'], verbose=False)
                        if result != 0 :
                            print(colorama.Fore.RED + ' ! Error ! S1 server response code : ', colorama.Fore.RESET,\
                                colorama.Fore.YELLOW + result, colorama.Fore.RESET, end='', sep='')
                        else :
                            print(colorama.Fore.GREEN + ' Success!', colorama.Fore.RESET, sep='')






if __name__ == '__main__':
    main()
    

